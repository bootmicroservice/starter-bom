package com.bootmicroservice.starterbom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarterBomApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarterBomApplication.class, args);
	}

}
